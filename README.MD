# Movie Microservice for MovieDB

#### Master Branch Coverage / Pipeline
| Pipeline | Coverage |Quality Gate Status|
|---       |---       |---                |
|[![pipeline status](https://gitlab.com/rheznandyae/moviedb-movie-microservice/badges/master/pipeline.svg)](https://gitlab.com/rheznandyae/moviedb-movie-microservice/-/commits/master)|[![coverage report](https://gitlab.com/rheznandyae/moviedb-movie-microservice/badges/master/coverage.svg)](https://gitlab.com/rheznandyae/moviedb-movie-microservice/-/commits/master)|

## Anggota Kelompok TK Advance Programming B4
|No | Nama                      | NPM       |
|---|---                        |---        |
|1. |Ageng Anugrah Wardoyo Putra| 1906398212|
|2. |Carissa Syieva Qalbiena    | 1806186875|
|3. |Ferdi Fadillah             | 1906351083|
|4. |Jovanta Anugerah Pelawi    | 1606875863|
|5. |Rheznandya Erwanto         | 1906318924|

## Project Description
Ini adalah aplikasi berbasis web yang memiliki fungsi sebagai platform untuk penggemar film yang ingin membuat review, artikel, berkomentar dan mencari rekomendasi film. 


