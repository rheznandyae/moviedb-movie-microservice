package kelompokb4.movieMicroservice.movie.service;


import kelompokb4.movieMicroservice.movie.core.Movie;

import java.util.List;

public interface MovieService {
    Movie getMovieByIdMovie(int idMovie);
    List<Movie> getListMovie();
    Movie updateMovie(int idMovie, Movie movie);
    Movie updateRatingMovie(int idMovie, double rating);

}
