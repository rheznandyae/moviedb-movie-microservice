package kelompokb4.movieMicroservice.movie.service;

import kelompokb4.movieMicroservice.movie.core.Movie;
import kelompokb4.movieMicroservice.movie.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService{
    @Autowired
    private MovieRepository movieRepository;

    @Override
    public Movie getMovieByIdMovie(int idMovie) {
        return movieRepository.findByIdMovie(idMovie);
    }

    @Override
    public List<Movie> getListMovie() {
        return movieRepository.findAll();
    }

    @Override
    public Movie updateMovie(int idMovie, Movie movie){
        movie.setIdMovie(idMovie);
        movieRepository.save(movie);
        return movie;
    }

    @Override
    public Movie updateRatingMovie(int idMovie, double rating){
        Movie movie = movieRepository.findByIdMovie(idMovie);
        movie.setRating(rating);
        movieRepository.save(movie);
        return movie;
    }
}
