package kelompokb4.movieMicroservice.movie.repository;

import kelompokb4.movieMicroservice.movie.core.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    Movie findByIdMovie(int idMovie);
    Movie findByTitle(String title);
}
