package kelompokb4.movieMicroservice.movie.core;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "movie")
@Data
@NoArgsConstructor
public class Movie {
    @Id
    private int idMovie;

    @Column(name = "title")
    private String title;

    @Column(name = "year")
    private String year;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "description")
    private String description;

    @Column(name = "url")
    private String url;

    @Column(name = "rating")
    private double rating = 0;

    public Movie (String title, String year, String description){
        this.title = title;
        this.year = year;
        this.description = description;
    }
}
