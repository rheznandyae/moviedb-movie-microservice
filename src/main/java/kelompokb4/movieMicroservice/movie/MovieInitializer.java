package kelompokb4.movieMicroservice.movie;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import kelompokb4.movieMicroservice.movie.core.Movie;
import kelompokb4.movieMicroservice.movie.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

@Component
public class MovieInitializer {

    @Autowired
    MovieRepository movieRepository;

    @PostConstruct
    public void init() throws IOException {
        var mapper = new ObjectMapper();
        JsonNode jsonObject;
        if(movieRepository.count() == 0){
            for (var j=1; j<= 10 ; j++){
//              Opening Connection to TheMovieDB Api
                var url = new URL("https://api.themoviedb.org/3/movie/popular?api_key=f0a1c60b0bae8fae2df20b86210a2389&page=" + j);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setDoInput(true);

                if(con.getResponseCode() == 200) {
                    var inline = new StringBuilder();
                    var scanner = new Scanner(url.openStream());
                    while (scanner.hasNext()) {
                        inline.append(scanner.nextLine());
                    }
                    scanner.close();
                    jsonObject = mapper.readTree(inline.toString());
                    JsonNode listMovie = jsonObject.get("results");
                    saveListOfMoviesToDB(listMovie);
                }
            }
        }
    }

    public void saveListOfMoviesToDB(JsonNode listMovie){
        for(var i = 0; i < listMovie.size(); i++){
            var id = listMovie.get(i).get("id").asInt();
            String title = listMovie.get(i).get("original_title").asText();
            String desc = listMovie.get(i).get("overview").asText();
            String poster = "" + "https://image.tmdb.org/t/p/w400/" + listMovie.get(i).get("poster_path").asText();
            JsonNode year = listMovie.get(i).get("release_date");
            if( year == null || year.asText().equals("")){
                continue;
            }
            var movie = new Movie(title, year.asText().substring(0,4), desc);
            movie.setIdMovie(id);
            movie.setUrl(poster);
            movieRepository.save(movie);
        }
    }
}
