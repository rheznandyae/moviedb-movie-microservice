package kelompokb4.movieMicroservice.movie.controller;

import kelompokb4.movieMicroservice.movie.core.Movie;
import kelompokb4.movieMicroservice.movie.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movie")
public class MovieController {
    @Autowired
    private MovieService movieService;


    @GetMapping(path = "/{idMovie}", produces = {"application/json"})
    public ResponseEntity getMovieById(@PathVariable("idMovie") int idMovie) {
        Movie movie = movieService.getMovieByIdMovie(idMovie);
        return movie != null ? ResponseEntity.ok(movie) : new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/", produces = {"application/json"})
    public ResponseEntity<Iterable<Movie>> getListMovie() {
        return ResponseEntity.ok(movieService.getListMovie());
    }

    @PutMapping(path = "/{idMovie}")
    public ResponseEntity<Movie> updateMovie(@PathVariable(value= "idMovie") int idMovie, @RequestBody Movie movie){
        return ResponseEntity.ok(movieService.updateMovie(idMovie, movie));
    }
    @PutMapping(path = "/rating/{idMovie}")
    public ResponseEntity<Movie> updateRatingMovie(@PathVariable(value= "idMovie") int idMovie, @RequestParam(required = false) double rating){
        return ResponseEntity.ok(movieService.updateRatingMovie(idMovie, rating));
    }

}
