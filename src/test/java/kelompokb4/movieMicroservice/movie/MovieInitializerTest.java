package kelompokb4.movieMicroservice.movie;


import kelompokb4.movieMicroservice.movie.repository.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class MovieInitializerTest {
    @Mock
    private MovieRepository movieRepository;

    @InjectMocks
    private MovieInitializer movieInitializer;

    @BeforeEach
    void init(){
        lenient().when(movieRepository.count()).thenReturn((long) 0);
    }

    @Test
    void testAsync() throws Exception{
        movieInitializer.init();
    }
}
