package kelompokb4.movieMicroservice.movie.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kelompokb4.movieMicroservice.movie.core.Movie;
import kelompokb4.movieMicroservice.movie.repository.MovieRepository;
import kelompokb4.movieMicroservice.movie.service.MovieService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = MovieController.class)
public class MovieControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    private MovieService movieService;

    @MockBean
    private MovieRepository movieRepository;

    private Movie movie;

    @BeforeEach
    public void init() {
        movie = new Movie();
        movie.setTitle("Mortal Kombat");
        movie.setIdMovie(1);
        movie.setUrl("https://image.tmdb.org/t/p/w400//nkayOAUBUu4mMvyNf9iHSUiPjF1.jpg");
        movie.setDescription("mortalkombat yeu");

        lenient().when(movieService.getMovieByIdMovie(1)).thenReturn(movie);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testListAllMovie() throws Exception{
        mockMvc.perform(get("/movie/")).andExpect(status().isOk());
    }

    @Test
    public void getMovieByIdTest() throws Exception{
        mockMvc.perform(get("/movie/1/")).andExpect(status().isOk());
    }

    @Test
    public void putMovieTest() throws Exception{
        mockMvc.perform(put("/movie/1").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(movie)))
                .andExpect(status().isOk());
    }

    @Test
    public void updateMovieRating() throws Exception{
        mockMvc.perform(put("/movie/rating/1?rating=6.5")).andExpect(status().isOk());
    }
}
