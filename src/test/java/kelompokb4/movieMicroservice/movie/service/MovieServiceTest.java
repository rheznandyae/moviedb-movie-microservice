package kelompokb4.movieMicroservice.movie.service;

import kelompokb4.movieMicroservice.movie.core.Movie;
import kelompokb4.movieMicroservice.movie.repository.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {

    @Mock
    private MovieRepository movieRepository;


    @InjectMocks
    private MovieServiceImpl movieServiceImpl;

    Movie movie;

    @BeforeEach
    void init(){
        movie = new Movie();
        movie.setTitle("Mortal Kombat");
        movie.setIdMovie(1);
        movie.setUrl("https://image.tmdb.org/t/p/w400//nkayOAUBUu4mMvyNf9iHSUiPjF1.jpg");
        movie.setDescription("mortalkombat yeu");
        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie);

        lenient().when(movieRepository.findByIdMovie(1)).thenReturn(movie);
        lenient().when(movieRepository.findByTitle("Mortal Kombat")).thenReturn(movie);
        lenient().when(movieRepository.findAll()).thenReturn(movies);

    }

    @Test
    public void testGetMovieById() {
        Movie getMovie = movieServiceImpl.getMovieByIdMovie(1);
        assertNotNull(getMovie);
    }

    @Test
    public void getListMovie() {
        List<Movie> getMovies = movieServiceImpl.getListMovie();
        assertNotNull(getMovies);
    }

    @Test
    public void updateRating() {
        Movie movieUpdated = movieServiceImpl.updateRatingMovie(movie.getIdMovie(), 6.4);
        assertNotNull(movieUpdated);
    }

    @Test
    public void updateMovie() {
        Movie movieUpdated = movieServiceImpl.updateMovie(1,movie);
        assertNotNull(movieUpdated);
    }

}